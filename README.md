# network-ctl


## TC Delay and Loss Control
  
  ./tc_ctl [delay(ms)] [loss(%)]


## TCP Congestion Control

  ./tcp_cc_ctl show
  
  ./tcp_cc_ctl switch [bbr/cubic]
